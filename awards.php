<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar -->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid" >
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="#">Awards and Citations</a></li>
				      <li><a href="visionmission.php">Vision-Mission and Quality Policies</a></li>
				      <li><a href="core.php">Core Values</a></li>
				      <li><a href="hymn.php">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>
		



	<div class="container">
		<div class="container-fluid">
			<h1 style="text-align: center;border-style: solid;border-color: #87ceeb">Awards and Citations</h1>
			<ul class="body">
				<li><h4 style="text-align: left;">Autonomous Institution by the Commission on Higher Education</h4></li><br>
				<li><h4 style="text-align: left;">First Catholic University in Region 2</h4></li><br>
				<li><h4 style="text-align: left;">First ISO 9001 Certified Catholic University and Private University in Asia</h4></li><br>
				<li><h4 style="text-align: left;">The only accredited school by Asian Association of Schools of Business International (AASBI) in the Philippines</h4></li><br>
				<li><h4 style="text-align: left;">One of the 12 participating schools in the Philippines for Asian International Mobility for Students (AIMS)</h4></li><br>
				<li><h4 style="text-align: left;">College and Basic Education Programs accredited by Philippine Accrediting Association of Schools, Colleges and Universities (PAASCU)</h4></li><br>
				<li><h4 style="text-align: left;">Graduate School Programs accredited by the Philippine Association of Colleges and Universities – Commission on Accreditation (PACUCOA)</h4></li><br>
				<li><h4 style="text-align: left;">Center of Excellence in Nursing</h4></li><br>
				<li><h4 style="text-align: left;">Center of Excellence in Teacher Education</h4></li><br>
				<li><h4 style="text-align: left;">Center of Development in Information Technology</h4></li><br>
				<li><h4 style="text-align: left;">Center of Development in Business education</h4></li><br>
				<li><h4 style="text-align: left;">Expanded Tertiary Education Equivalency and Accreditation Program (ETEEAP) Participating Institution</h4></li><br>
				<li><h4 style="text-align: left;">Center for Teacher Training Institution</h4></li><br>
				<li><h4 style="text-align: left;">One of only 11 Universities in the Philippines granted a Knowledge for Development Center by World Bank</h4></li><br>
				<li><h4 style="text-align: left;">Most Outstanding Student Services of the Philippines</h4></li><br>
				<li><h4 style="text-align: left;">Catholic Cultural Center by the Catholic Bishops’ Conference of the Philippines</h4></li><br>
	 			<li><h4 style="text-align: left;">Historical Site by the National Historical Commission of the Philippines</h4></li><br>
				<li><h4 style="text-align: left;">Most Environment-Friendly and Sustainable School in Region II</h4></li><br>
				<li><h4 style="text-align: left;">Producer of Winners in  various searches for Outstanding Students and Professionals</h4></li><br>
				<li><h4 style="text-align: left;">Producer of Board Topnotchers and Passers in Licensure Examinations</h4></li><br>
				<li><h4 style="text-align: left;">UNESCO Associated Schools Project Netword (ASPnet) Affiliate</h4></li><br>
			</ul>
		</div>
	</div>

	<div class="container">
		<div class="footer">
	 	 	<div class="jumbotron">
	 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
	 	 	<div class="row">
	 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">ISO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">IAO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">AASBI</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PAASCU</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PACUCOA</h4>
	    		</div>
	  		</div>	
	</div>



</body>
</html>